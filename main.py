inflacja = [1.59282448436825, -0.453509101198007, 2.32467171712441,
            1.26125440724877, 1.78252628571251, 2.32938454145522,
            1.50222984223283, 1.78252628571251, 2.32884899407637,
            3.32884899407637, 4.32884899407637, 5.32884899407637,
            6.32884899407637, 7.32884899407637, 8.32884899407637,
            9.32884899407637, 10.3288489940764, 11.3288489940764,
            12.3288489940764, 13.3288489940764, 14.3288489940764,
            15.3288489940764, 16.32884899407641, 7.3288489940764]

print('Wpisz kwotę kredytu:')
wys_kred = float(input())

'''
print('Wybierz oprocentowanie kredytu:')
oproc_kred = input()

print('Wybierz kwotę raty:')
kwota_raty = input()'''

print('Wysokość początkowego kredytu to: {}'.format(wys_kred))

'''
print('Oprocentowanie kredytu to: {}'.format(oproc_kred))
print('Kwota raty to: {}'.format(kwota_raty))'''


'''
Wzor na obliczenie rat kredytu:
(1 + ((inflacja[licznik]+3)/1200)) * ostatnia_rata - 200
'''

ostatnia_rata = wys_kred

licznik = 0

while licznik < 24:
    nawias3 = inflacja[licznik] + 3
    nawias2 = nawias3 / 1200
    nawias1 = 1 + nawias2
    mnozenie = nawias1 * ostatnia_rata
    aktualna_rata = mnozenie - 200
    mniej = ostatnia_rata - aktualna_rata
    x = str(round(aktualna_rata, 2))
    y = str(round(mniej, 2))
    print("Twoja pozostała kwota kredytu to " + x + ",to " + y +
          " mniej niż w poprzednim miesiącu.")
    ostatnia_rata = aktualna_rata
    licznik += 1
